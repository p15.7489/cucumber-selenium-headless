package de.p15.cucumber.selenium.test.driver.headless;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class HeadlessTestSteps {
	private HtmlUnitDriver driver = new HtmlUnitDriver();

	@Given("I am on the search page {string}")
	public void i_am_on_the_search_page(String searchPageUrl) {
		driver.get(searchPageUrl);
	}

	@When("I search for {string}")
	public void i_search_for(String query) throws Exception {
		WebElement element = driver.findElement(By.name("q"));
		// Enter something to search for
		element.sendKeys(query);
		// Now submit the form. WebDriver will find the form for us from the element
		element.submit();
	}
	
	@When("I search on wikipedia for {string}")
	public void i_search_on_wikipedia_for(String query) {
		WebElement element = driver.findElement(By.id("searchInput"));
		// Enter something to search for
		element.sendKeys(query);
		// Now submit the form. WebDriver will find the form for us from the element
		element.submit();
	}

	@Then("the page title should start with {string}")
	public void the_page_title_should_start_with(final String titleStartsWith) throws Exception {
		// Google's search is rendered dynamically with JavaScript
		// Wait for the page to load timeout after ten seconds

		new WebDriverWait(driver, Duration.ofSeconds(10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getTitle().contains(titleStartsWith);
			}
		});
	}

	@After()
	public void closeBrowser() {
		driver.quit();
	}
}
