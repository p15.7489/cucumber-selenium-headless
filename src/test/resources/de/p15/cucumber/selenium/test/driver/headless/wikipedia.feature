Feature: wikipedia search
  
  Scenario: Finding something
   Given I am on the search page "https://de.wikipedia.org/wiki/Wikipedia:Hauptseite"
   When I search on wikipedia for "Behavior Driven Development"
   Then the page title should start with "Behavior Driven Development"