Feature: duckduckgo search
  
  Scenario: Finding something
   Given I am on the search page "https://duckduckgo.com"
   When I search for "Mettwurst"
   Then the page title should start with "Mettwurst"